package pt.luisbilhoto.funcionarios;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class ViewActivity extends AppCompatActivity {
    private BluetoothAdapter bluetoothAdapter;
    private TextView viewCode, viewName, viewMAC, viewDate;
    private Button viewPhone, viewEmail;
    private Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        setTitle(R.string.menu_view);

        // Display a Custom Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Bluetooth Adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) { finish(); }

        // Assign View Elements
        viewCode  = findViewById(R.id.viewCode);
        viewName  = findViewById(R.id.viewName);
        viewMAC   = findViewById(R.id.viewMAC);
        viewDate  = findViewById(R.id.viewDate);
        viewPhone = findViewById(R.id.viewPhone);
        viewEmail = findViewById(R.id.viewEmail);

        // Add Funcionario Button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            Intent viewIntent = new Intent(ViewActivity.this, EditActivity.class);
            viewIntent.putExtra("code", viewCode.getText().toString());
            viewIntent.putExtra("name", viewName.getText().toString());
            viewIntent.putExtra("mac", viewMAC.getText().toString());
            viewIntent.putExtra("phone", viewPhone.getText().toString());
            viewIntent.putExtra("email", viewEmail.getText().toString());
            startActivityForResult(viewIntent, 1);
        });

        // Database Initialization
        db = new Database(ViewActivity.this);

        // Initial Display
        showFuncionario(getIntent().getExtras().getString("name"));

        // Phone Button (Send SMS Intent)
        viewPhone.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("sms:"+viewPhone.getText().toString()));
            intent.putExtra("address", viewPhone.getText().toString());
            if (intent.resolveActivity(getPackageManager()) != null) { startActivity(intent); }
        });

        // Email Button (Send Email Intent)
        viewEmail.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"+viewEmail.getText().toString()));
            intent.putExtra(Intent.EXTRA_EMAIL, viewEmail.getText().toString());
            if (intent.resolveActivity(getPackageManager()) != null) { startActivity(intent); }
        });
    }

    @Override
    protected void onResume() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            startActivity(new Intent(ViewActivity.this, StatusActivity.class));
        }
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            showFuncionario(data.getStringExtra("name"));
        }
    }

    // Display Funcionario Data
    public void showFuncionario(String name) {
        Cursor data = db.getFuncionario(name);
        viewCode.setText(data.getString(data.getColumnIndex("codigo")));
        viewName.setText(data.getString(data.getColumnIndex("nome")));
        viewMAC.setText(data.getString(data.getColumnIndex("mac")));
        viewDate.setText(data.getString(data.getColumnIndex("data")));
        if (data.getString(data.getColumnIndex("telefone")).length() > 0) {
            viewPhone.setText(data.getString(data.getColumnIndex("telefone")));
            viewPhone.setVisibility(View.VISIBLE);
        } else {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)viewEmail.getLayoutParams();
            params.topMargin = 86;
            viewEmail.setLayoutParams(params);
            viewPhone.setVisibility(View.GONE);
        }
        if (data.getString(data.getColumnIndex("email")).length() > 0) {
            viewEmail.setText(data.getString(data.getColumnIndex("email")));
            viewEmail.setVisibility(View.VISIBLE);
        } else {
            viewEmail.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_delete) {
            new AlertDialog.Builder(ViewActivity.this)
                .setTitle(R.string.delete_title)
                .setMessage(R.string.delete_message)
                .setPositiveButton(R.string.input_yes, (dialog, which) -> {
                    if (db.deleteFuncionario(viewName.getText().toString()) < 1) {
                        Snackbar.make(findViewById(R.id.toolbar), R.string.delete_error, Snackbar.LENGTH_LONG)
                            .setAction("Error", null).show()
                        ;
                    } else {
                        finish();
                    }
                })
                .setNegativeButton(R.string.input_no, null)
                .show()
            ;
        }
        return super.onOptionsItemSelected(item);
    }
}
