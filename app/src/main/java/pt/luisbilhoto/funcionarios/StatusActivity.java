package pt.luisbilhoto.funcionarios;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class StatusActivity extends AppCompatActivity {
    Button btnActivate, btnExit;
    ImageView image;
    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        // Assign View IDs
        image       = findViewById(R.id.imageStatus);
        message     = findViewById(R.id.displayStatus);
        btnActivate = findViewById(R.id.btnActivate);
        btnExit     = findViewById(R.id.btnExit);

        btnExit.setOnClickListener(v -> finishAffinity());
    }

    @Override
    protected void onResume() {
        super.onResume();
        onUpdate();
    }

    protected void onUpdate() {
        if (!isLocationEnabled()) {
            image.setImageResource(R.drawable.ic_location_disable);
            message.setText(R.string.status_location_disabled);
            btnActivate.setOnClickListener(v -> {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            });
        } else if (bluetoothStatus() == 0) {
            image.setImageResource(R.drawable.ic_bluetooth_disabled);
            message.setText(R.string.status_bluetooth_disabled);
            btnActivate.setOnClickListener(v -> {
                startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
            });
        } else if (bluetoothStatus() == -1) {
            image.setImageResource(R.drawable.ic_bluetooth_disabled);
            message.setText(R.string.status_bluetooth_unsupported);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) btnExit.getLayoutParams();
            params.topMargin = 86;
            btnExit.setLayoutParams(params);
            btnActivate.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private int bluetoothStatus() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null)
            return -1;
        if (bluetoothAdapter.isEnabled())
            return 1;
        return 0;
    }
}
