package pt.luisbilhoto.funcionarios;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;

public class AddActivity extends AppCompatActivity {
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<String> macs = new ArrayList<String> ();
    private EditText inputCode, inputName, inputPhone, inputEmail;
    private MenuItem actionAdd;
    private Spinner inputMac;
    private Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        setTitle(R.string.menu_add);

        // Display Custom Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_close);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Bluetooth Adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Bluetooth Receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(broadcastReceiver, filter);

        // Bluetooth Start Discovery
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.startDiscovery();
        } else { finish(); }

        // Assign Input Fields
        inputCode   = findViewById(R.id.inputCode);
        inputName   = findViewById(R.id.inputName);
        inputMac    = findViewById(R.id.inputMac);
        inputPhone  = findViewById(R.id.inputPhone);
        inputEmail  = findViewById(R.id.inputEmail);

        // Show MACs Loading Message
        macs.add(getString(R.string.status_bluetooth_loading));
        displayMACs();

        // Database Initialization
        db = new Database(AddActivity.this);
    }

    @Override
    protected void onResume() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            startActivity(new Intent(AddActivity.this, StatusActivity.class));
        }
        super.onResume();
    }

    private void displayMACs() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String> (
            AddActivity.this, android.R.layout.simple_spinner_item, macs
        );
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inputMac.setAdapter(arrayAdapter);
        macs = new ArrayList<String> ();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        actionAdd = menu.findItem(R.id.action_add);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_add) {
            if (
                db.addFuncionario(
                    inputCode.getText().toString(),
                    inputName.getText().toString(),
                    inputMac.getSelectedItem().toString(),
                    inputPhone.getText().toString(),
                    inputEmail.getText().toString()
                ) < 0
            ) {
                Snackbar.make(findViewById(R.id.toolbar), R.string.insert_error, Snackbar.LENGTH_LONG)
                    .setAction("Error", null).show()
                ;
            } else {
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                macs.add(device.getAddress());
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                actionAdd.setVisible(true);
                actionAdd.setEnabled(true);
                displayMACs();
            }
        }
    };

    @Override
    protected void onPause() {
        if (bluetoothAdapter != null && bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }
}
