package pt.luisbilhoto.funcionarios;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;

public class EditActivity extends AppCompatActivity {
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<String> macs = new ArrayList<String> ();
    private EditText inputCode, inputName, inputPhone, inputEmail;
    private MenuItem actionAdd;
    private Spinner inputMAC;
    private Database db;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        setTitle(R.string.menu_edit);

        // Display Custom Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_close);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Bluetooth Adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Bluetooth Receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(broadcastReceiver, filter);

        // Bluetooth Start Discovery
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.startDiscovery();
        } else { finish(); }

        // Assign Input Fields
        inputCode   = findViewById(R.id.inputCode);
        inputName   = findViewById(R.id.inputName);
        inputMAC    = findViewById(R.id.inputMac);
        inputPhone  = findViewById(R.id.inputPhone);
        inputEmail  = findViewById(R.id.inputEmail);

        Bundle extras = getIntent().getExtras();
        inputCode.setText(extras.getString("code"));
        inputName.setText(extras.getString("name"));
        inputPhone.setText(extras.getString("phone"));
        inputEmail.setText(extras.getString("email"));
        name = extras.getString("name");

        // Display MAC Addresses
        macs.add(extras.getString("mac"));
        displayMACs();

        // Database Initialization
        db = new Database(EditActivity.this);
    }

    @Override
    protected void onResume() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            startActivity(new Intent(EditActivity.this, StatusActivity.class));
        }
        super.onResume();
    }

    private void displayMACs() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String> (
            EditActivity.this, android.R.layout.simple_spinner_item, macs
        );
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inputMAC.setAdapter(arrayAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        actionAdd = menu.findItem(R.id.action_add);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_add) {
            if (
                db.updateFuncionario(
                    name,
                    inputCode.getText().toString(),
                    inputName.getText().toString(),
                    inputMAC.getSelectedItem().toString(),
                    inputPhone.getText().toString(),
                    inputEmail.getText().toString()
                ) < 1
            ) {
                Snackbar.make(findViewById(R.id.toolbar), R.string.update_error, Snackbar.LENGTH_LONG)
                    .setAction("Error", null).show()
                ;
            } else {
                setResult(
                    Activity.RESULT_OK,
                    new Intent().putExtra("name", inputName.getText().toString())
                );
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                macs.add(device.getAddress());
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                actionAdd.setVisible(true);
                actionAdd.setEnabled(true);
                displayMACs();
            }
        }
    };

    @Override
    protected void onPause() {
        if (bluetoothAdapter != null && bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }
}
