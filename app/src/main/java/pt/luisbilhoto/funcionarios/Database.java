package pt.luisbilhoto.funcionarios;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {
    private final SQLiteDatabase db;

    // Database Helper
    //==========================================================================================
    public Database(Context context) throws SQLException {
        super(context, "funcionarios.db", null, 1);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
            "CREATE TABLE funcionario (" +
                "codigo TEXT NOT NULL PRIMARY KEY," +
                "nome TEXT NOT NULL UNIQUE," +
                "mac TEXT NOT NULL," +
                "telefone TEXT," +
                "email TEXT," +
                "imagem TEXT," +
                "data DATE DEFAULT (datetime('now','localtime'))" +
            ");"
        );
        db.execSQL(
            "CREATE TABLE assiduidade (" +
                "funcionario TEXT NOT NULL," +
                "data DATE DEFAULT (datetime('now','localtime'))," +
                "FOREIGN KEY (funcionario) REFERENCES funcionario(codigo)," +
                "PRIMARY KEY (funcionario, data)" +
            ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS funcionario;");
        db.execSQL("DROP TABLE IF EXISTS assiduidade;");
        onCreate(db);
    }

    // Database Manager
    //==========================================================================================
    public long addFuncionario(String code, String name, String mac, String phone, String email) {
        ContentValues content = new ContentValues();
        content.put("codigo", code);
        content.put("nome", name);
        content.put("mac", mac);
        content.put("telefone", phone);
        content.put("email", email);
        return db.insert("funcionario", null, content);
    }

    public int updateFuncionario(String oldName, String code, String newName, String mac, String phone, String email) {
        ContentValues content = new ContentValues();
        content.put("codigo", code);
        content.put("nome", newName);
        content.put("mac", mac);
        content.put("telefone", phone);
        content.put("email", email);
        return db.update(
            "funcionario", content,
            "nome = ?", new String[] {
                oldName
            });
    }

    public Cursor getFuncionario(String name) {
        Cursor cursor = db.query(
            "funcionario", new String[] {
                "rowid _id",
                "codigo",
                "nome",
                "mac",
                "telefone",
                "email",
                "imagem",
                "data"
            },
            "nome = ?", new String[] {
                name
            },
            null,
            null,
            null,
            "1"
        );
        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    public Cursor getFuncionarios() {
        Cursor cursor = db.query(
            "funcionario LEFT JOIN assiduidade ON assiduidade.funcionario = funcionario.codigo", new String[] {
                "funcionario.rowid _id",
                "funcionario.nome nome",
                "funcionario.mac mac",
                "CASE WHEN assiduidade.data IS NULL THEN '0000-00-00 00:00:00' ELSE assiduidade.data END data",
                "CASE WHEN assiduidade.data IS NOT NULL AND assiduidade.data > datetime('now','-15 minutes','localtime') THEN " + R.drawable.ic_list_online + " ELSE " + R.drawable.ic_list_offline + " END status"
            },
            null,
            null,
            "funcionario.codigo",
            null,
            null
        );
        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    public Cursor searchFuncionarios(String name) {
        Cursor cursor = db.query(
            "funcionario LEFT JOIN assiduidade ON assiduidade.funcionario = funcionario.codigo", new String[] {
                "funcionario.rowid _id",
                "funcionario.nome nome",
                "funcionario.mac mac",
                "CASE WHEN assiduidade.data IS NULL THEN '0000-00-00 00:00:00' ELSE assiduidade.data END data",
                "CASE WHEN assiduidade.data IS NOT NULL AND assiduidade.data > datetime('now','-15 minutes','localtime') THEN " + R.drawable.ic_list_online + " ELSE " + R.drawable.ic_list_offline + " END status"
            },
            "nome LIKE ?", new String[] {
                "%" + name + "%"
            },
            "funcionario.codigo",
            null,
            null
        );
        if (cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    public int deleteFuncionario(String name) {
        return db.delete(
            "funcionario",
            "nome = ?", new String[] {
                name
            });
    }

    public boolean addAssiduidade(String mac) {
        Cursor cursor = db.query(
            "funcionario", new String[] {
                "codigo",
            },
            "mac = ?", new String[] {
                mac
            },
            null,
            null,
            null
        );
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ContentValues content = new ContentValues();
                content.put("funcionario", cursor.getString(cursor.getColumnIndex("codigo")));
                db.insert("assiduidade", null, content);
                cursor.moveToNext();
            }
            return true;
        }
        return false;
    }
}
