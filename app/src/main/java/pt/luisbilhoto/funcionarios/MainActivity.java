package pt.luisbilhoto.funcionarios;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {
    private BluetoothAdapter bluetoothAdapter;
    private ListView list;
    private Database db;
    private String search = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.app_name);

        // Display Custom Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Bluetooth Adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Bluetooth Receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(broadcastReceiver, filter);

        // Add Funcionario Button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, AddActivity.class)));

        // View Funcionario (Row Click)
        list = findViewById(R.id.list);
        list.setOnItemClickListener((parent, view, position, id) -> {
            TextView rowName = view.findViewById(R.id.rowName);
            Intent viewIntent = new Intent(MainActivity.this, ViewActivity.class);
            viewIntent.putExtra("name", rowName.getText().toString());
            startActivity(viewIntent);
        });

        // Database Initialization
        db = new Database(MainActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayList();
        onUpdate();
    }

    protected void onUpdate() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            startActivity(new Intent(MainActivity.this, StatusActivity.class));
        } else if (!bluetoothAdapter.isDiscovering()) { bluetoothAdapter.startDiscovery(); }
    }

    // Display Funcionarios List
    private void displayList() {
        Cursor data = db.getFuncionarios();
        if (search.length() > 0) { data = db.searchFuncionarios(search); }
        SimpleCursorAdapter listCursorAdapter = new SimpleCursorAdapter(
            MainActivity.this, R.layout.list_row, data,
            new String[]{"nome", "mac", "data", "status"},
            new int[]{R.id.rowName, R.id.rowMAC, R.id.rowDate, R.id.rowStatus}
        );
        list.setAdapter(listCursorAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_sync) {
            onUpdate();
        } else if (id == R.id.action_search) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            LayoutInflater layoutInflater = MainActivity.this.getLayoutInflater();
            View dialogView = layoutInflater.inflate(R.layout.dialog_search, null);
            EditText inputSearch = dialogView.findViewById(R.id.inputSearch);
            Button btnSearch = dialogView.findViewById(R.id.btnSearch);
            Button btnCancel = dialogView.findViewById(R.id.btnCancel);
            btnSearch.setOnClickListener(v -> {
                search = inputSearch.getText().toString();
                displayList();
                alertDialog.dismiss();
            });
            btnCancel.setOnClickListener(v -> alertDialog.dismiss());
            inputSearch.setText(search);
            alertDialog.setView(dialogView);
            alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                db.addAssiduidade(device.getAddress());
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                displayList();
            }
        }
    };

    @Override
    protected void onPause() {
        if (bluetoothAdapter != null && bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }
}
